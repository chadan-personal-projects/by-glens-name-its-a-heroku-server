"use strict";

const express = require("express");
const handlebars = require("express-handlebars");
const path = require("path");

const app = express();
const port = process.env.PORT || "8000";

const handlebarsInstance = handlebars.create({
	extname: "hbs",
});

app.engine("hbs", handlebarsInstance.engine)
        .set("views", path.join(__dirname, "views"))
		.set("view engine", "hbs")
		
		.use(express.static(path.join(__dirname, "public")))
		.use("/place", express.Router().get("/", (req, res) => {
			res.render("place", {
				title: "abc",
				layout: false,
			});
		}))

		.listen(port, console.log);

// app.get("/", (req, res) => {
// 	res.render("index");
// });